
include ./etc/help.mk

UID:=$(shell id -u)
GID:=$(shell id -g)
PWD:=$(shell pwd)

# .PHONY: setup
# setup: ##@setup builds the docker image for the cli make cmd
# 	docker ...

.PHONY: cli
cli: ##@setup set up a docker container with mounted source where you can execute all elixir commands
	# docker run -it --rm  -u $(UID):$(GID) -v $(PWD):/source -w /source golang:1.10.3 bash
	docker run -it --rm -v $(PWD):/source -w /source golang:1.10.3 bash
