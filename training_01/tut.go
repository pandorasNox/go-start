package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

func squareroot() {
	fmt.Println("The square root of 4 is:", math.Sqrt(4))
}

func rndnumber() {
	fmt.Println("A random number (without seed) in [0,n) with n = 100, 0 is included, n not:", rand.Intn(100))
}

func rndnumberwithseed() {
	rand.Seed(time.Now().UTC().UnixNano())
	fmt.Println("A random number (with seed) in [0,n) with n = 100, 0 is included, n not:", rand.Intn(100))
}

func main() {
	fmt.Println("Hello World")
	squareroot()
	rndnumber()
	rndnumberwithseed()
}
