# go-start
golang learning and setup project

## cheat sheet

```
# run go code
go run mygofile.go

# format code
go fmt mygofile.go

# use docs
godocs math/rand Intn
```